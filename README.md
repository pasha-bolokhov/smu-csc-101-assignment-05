# Assignment 5

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a05`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```

Each file must have a
```shell
#!/usr/bin/env python3
```
line at the very top, and must be made executable with a command
```shell
chmod a+x p1.py
```
(same for `p2.py` ...)




### Problem 1 (*20%*)

Write a program that reads a file (it could be the same file you are editing, `p1.py`), and writes
the content into a new file `upper.out` where all the characters are in upper case

*Hint:* you will need to use `write()` and `close()` methods of the filehandle that you will create (by calling `open()`)




### Problem 2 (*80%*)

Download the file `heroes.txt` attached to the assignment.
You will need to place this file in the same folder where your program is.
File has a format `Name` and `number`,
the number represents power.
The name and the (integer) power are separated by some amount of space.
Importantly, any line that begins with a hash `'#'` are comments and they need to be ingored.
Write a program that reads from that file, and prints out only the name of the hero with the strongest power. That name should be capitalized (not uppercase, but capitalized, as in `'Galadriel'`)
